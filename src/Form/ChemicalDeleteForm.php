<?php

namespace Drupal\chemical_entity\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Chemical entities.
 *
 * @ingroup chemical_entity
 */
class ChemicalDeleteForm extends ContentEntityDeleteForm {


}
