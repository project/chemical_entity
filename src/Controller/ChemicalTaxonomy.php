<?php
namespace Drupal\chemical_entity\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Table of contents for the FLOT Example Module.
 */
class ChemicalTaxonomy extends ControllerBase {

  /**
   * Function content.
   */
  public function content($tid) {
    $ids = \Drupal::entityQuery('chemical')
      ->condition('tid', $tid)
      ->execute();
    $controller = \Drupal::entityManager()->getStorage('chemical');
    $entities = $controller->loadMultiple($ids);

    $entity_manager = \Drupal::entityManager();
    $term = $entity_manager->getStorage('taxonomy_term')->load($tid);

    $tree = \Drupal::entityManager()->getStorage('taxonomy_term')->loadTree('chemical', $tid, 1, FALSE);
    //print_r($tree);
    $output[] = [
      '#theme' => 'chemical_taxonomy',
      '#term' => $term,
      '#chemicals' => $entities,
      '#children' => $tree,
    ];
    return $output;
  }

}
