<?php
namespace Drupal\chemical_entity\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Table of contents for the FLOT Example Module.
 */
class Chemical extends ControllerBase {

  /**
   * Function content.
   */
  public function content($id) {
    $entity_type = 'chemical';
    $controller = \Drupal::entityManager()->getStorage($entity_type);
    $chemical_entity = $controller->load($id);
    
    $smiles = $chemical_entity->getSmiles(); 
    $info = [
      'width' => 200,
      'height' => 200,
      'debug' => false,
      'j2sPath' => '/modules/custom/jmol/lib/j2s',
      'color' => "0xC0C0C0",
      'disableJ2SLoadMonitor' => true,
      'disableInitialConsole' => true,
      'use' => "HTML5",
      'script' => 'load $' . $smiles,
    ];
    $output[] = [
      '#type' => 'jmol',
      '#info' => $info,
    ];

    $output[] = [
      '#theme' => 'chemical',
      '#chemical' => $chemical_entity,
    ];
    return $output;
  }

}
