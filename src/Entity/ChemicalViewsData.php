<?php

namespace Drupal\chemical_entity\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Chemical entities.
 */
class ChemicalViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['chemical']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Chemical'),
      'help' => $this->t('The Chemical ID.'),
    );

    return $data;
  }

}
