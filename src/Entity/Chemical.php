<?php

namespace Drupal\chemical_entity\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Chemical entity.
 *
 * @ingroup chemical_entity
 *
 * @ContentEntityType(
 *   id = "chemical",
 *   label = @Translation("Chemical"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\chemical_entity\ChemicalListBuilder",
 *     "views_data" = "Drupal\chemical_entity\Entity\ChemicalViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\chemical_entity\Form\ChemicalForm",
 *       "add" = "Drupal\chemical_entity\Form\ChemicalForm",
 *       "edit" = "Drupal\chemical_entity\Form\ChemicalForm",
 *       "delete" = "Drupal\chemical_entity\Form\ChemicalDeleteForm",
 *     },
 *     "access" = "Drupal\chemical_entity\ChemicalAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\chemical_entity\ChemicalHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "chemical",
 *   admin_permission = "administer chemical entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "smiles" = "smiles",
 *     "boiling_point" = "boiling_point",
 *     "tid" = "tid",
 *     "alternate_tid" = "alternate_tid",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/chemical_entity/chemical/{chemical}",
 *     "add-form" = "/admin/structure/chemical_entity/chemical/add",
 *     "edit-form" = "/admin/structure/chemical_entity/chemical/{chemical}/edit",
 *     "delete-form" = "/admin/structure/chemical_entity/chemical/{chemical}/delete",
 *     "collection" = "/admin/structure/chemical_entity/chemical",
 *   },
 *   field_ui_base_route = "chemical.settings"
 * )
 */
class Chemical extends ContentEntityBase implements ChemicalInterface {

  /**
   * {@inheritdoc}
   */
  public function getAlternateNames() {
    return $this->get('alternate_names')->values;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSmiles() {
    return $this->get('smiles')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getBoilingPoint() {
    return $this->get('boiling_point')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setBoilingPoint($bp) {
    $this->set('boiling_point', $bp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Chemical entity.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['alternate_names'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Alternate Name'))
      ->setDescription(t('Alternate Names.'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setRequired(FALSE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['smiles'] = BaseFieldDefinition::create('string')
      ->setLabel(t('SMILES'))
      ->setDescription(t('The SMILES structure.'))
      ->setSettings(array(
        'max_length' => 100,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setRequired(FALSE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['boiling_point'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Boiling Point'))
      ->setDescription(t('The boiling point of the compound.'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'float',
        'weight' => 0,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setRequired(FALSE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['tid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Chemical Family'))
      ->setDescription(t('The family to which this chemical belongs.'))
      ->setSettings(['target_type' => 'taxonomy_term'])
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ),
      ))
      ->setRequired(FALSE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['alternate_tid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Chemical Family'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDescription(t('The family to which this chemical belongs.'))
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 0,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ),
      ))
      ->setRequired(FALSE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
