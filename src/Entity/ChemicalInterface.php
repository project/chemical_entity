<?php

namespace Drupal\chemical_entity\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining Chemical entities.
 *
 * @ingroup chemical_entity
 */
interface ChemicalInterface extends ContentEntityInterface {

  /** Gets the Chemical name.
   *
   * @return string
   *   Name of the Chemical.
   */
  public function getName();

  /**
   * Sets the Chemical name.
   *
   * @param string $name
   *   The Chemical name.
   *
   * @return \Drupal\chemical_entity\Entity\ChemicalInterface
   *   The called Chemical entity.
   */
  public function setName($name);

  /**
   * Gets the Chemical creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Chemical.
   */
  public function getBoilingPoint();

  /**
   * Sets the Chemical creation timestamp.
   *
   * @param int $timestamp
   *   The Chemical creation timestamp.
   *
   * @return \Drupal\chemical_entity\Entity\ChemicalInterface
   *   The called Chemical entity.
   */
  public function setBoilingPoint($timestamp);
}
