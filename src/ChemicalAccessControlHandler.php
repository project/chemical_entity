<?php

namespace Drupal\chemical_entity;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Chemical entity.
 *
 * @see \Drupal\lab_system\Entity\Chemical.
 */
class ChemicalAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\lab_system\Entity\ChemicalInterface $entity */
    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view published chemical entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit chemical entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete chemical entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add chemical entities');
  }

}
