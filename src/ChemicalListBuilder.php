<?php

namespace Drupal\chemical_entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Chemical entities.
 *
 * @ingroup chemical_entity
 */
class ChemicalListBuilder extends EntityListBuilder {

  use LinkGeneratorTrait;

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Chemical ID');
    $header['name'] = $this->t('Name');
    $header['boiling_point'] = $this->t('Boiling Point');
    $header['smiles'] = $this->t('SMILES');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\chemical_entity\Entity\Chemical */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.chemical.edit_form', array(
          'chemical' => $entity->id(),
        )
      )
    );
    $row['boiling_point'] = $entity->getBoilingPoint();
    $row['smiles'] = $entity->getSmiles();
    return $row + parent::buildRow($entity);
  }

}
